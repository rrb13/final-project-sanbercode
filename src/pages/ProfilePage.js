import React from "react";
import { Helmet } from "react-helmet";
import Profile from "../components/Profile";
import Layout from "../layout/Layout";

function ProfilePage() {
  return (
    <div>
      {/* NAVBAR */}
      <Helmet>
        <title>Profile Page</title>
      </Helmet>
      <Layout>
        <Profile />
      </Layout>
    </div>
  );
}

export default ProfilePage;
