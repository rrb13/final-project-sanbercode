import React from "react";
import RegisterForm from "../components/RegisterForm";
import Layout from "../layout/Layout";

function RegisterPage() {
  return (
    <div>
      <Layout>
        <RegisterForm />
      </Layout>
    </div>
  );
}

export default  RegisterPage;
