import React, { useContext, useEffect, useState } from "react";
import Product from "../components/Product";
import { GlobalContext } from "../context/GlobalContext";
import Layout from "../layout/Layout";
import {Helmet} from "react-helmet"
import { Link, useParams } from "react-router-dom";
import axios from "axios";

function DetailPage() {
    const { productId } = useParams();
  const [input,setInput] = useState({
        name:"", //nama barang
        stock:0, //stock barang
        harga_display:0, //harga barang
        is_diskon:false, //status diskon
        harga_diskon_display:0,
        category:"", //kategori barang
        image_url:"", //url gambar
        description:"", //deskripsi
    })

    // melakukan fetch product detail
  const fetchproductDetail = async () => {
    try {
      // fetch data menggunakan axios
      const response = await axios.get(
        `https://api-project.amandemy.co.id/api/final/products/${productId}`
      );
      console.log(response.data.data)
      const product = response.data.data;
      // kita memasukkan data dari server ke dalam form nya
      setInput({
        name: product.name,
        stock: product.stock,
        harga_display: product.harga_display,
        is_diskon:product.is_diskon,
        harga_diskon_display:product.harga_diskon_display,
        category:product.category,
        image_url: product.image_url,
        description:product.description,
      });
    } catch (error) {
      alert(`Terjadi Sesuatu Error : ${error}`);
      console.log(error)
    }
  };

    useEffect(() => {
      fetchproductDetail();
    }, []);


  return (
    <div>
      {/* NAVBAR */}
      <Layout>
        <Helmet>
            <title>Products Detail - Man Cave</title>
        </Helmet>
        <div className="max-w-7xl mx-auto my-10">
        
        {/* <!-- menu --> */}
        <div class="flex justify-between my-5">
            <span class="self-center text-lg text-orange-300 font-semibold">
                <Link to="/">
                    <span class="text-orange-400">Home </span>
                </Link> /
                <Link to="/product">
                    <span class="text-orange-400"> Product </span>
                </Link> /
                <span class="text-orange-300"> Detail</span></span>
        </div>
        {/* <!-- list of card products --> */}
        <div class="grid grid-cols-3 gap-2 max-md:grid-cols-1 max-md:gap-0 pt-2">
            <div class="bg-white border border-gray-200 rounded-xl max-md:rounded-b-none h-[28rem]">
                <img class="rounded-xl max-md:rounded-b-none h-full w-full object-cover" src={input.image_url} alt="" />
            </div>
            <div class="col-span-2 grid content-start bg-white border border-gray-200 rounded-xl max-md:rounded-t-none p-5">
                <div>
                    <h5 class="text-4xl font-bold tracking-tight text-gray-900">{input.name}</h5>
                    <h5 class="mb-2 text-md font-semibold tracking-tight text-gray-400 ">{input.category}</h5> 
                </div>
                {input.is_diskon === true 
                ? <div className="my-3">
                    <h5 class="mb-2 text-xl font-semibold tracking-tight text-gray-900 line-through">{input.harga_display}</h5>
                    <h5 class="mb-2 text-2xl font-semibold tracking-tight text-red-400">{input.harga_diskon_display}</h5>
                  </div>
                : <h5 class="mb-2 text-2xl font-semibold tracking-tight text-gray-900 my-3">{input.harga_display}</h5>
                }
                <p class="mb-2 text-lg text-gray-900 my-5">{input.description}</p>
                <h5 class="mb-2 text-xl font-semibold tracking-tight text-blue-400 my-5">Stock : {input.stock}</h5>
            </div>

        </div>
        </div>
      </Layout>
      
    </div>
  );
}

export default DetailPage;
