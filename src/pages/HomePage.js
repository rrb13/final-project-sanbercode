import React, { useContext, useEffect } from "react";
import Product from "../components/Product";
import { GlobalContext } from "../context/GlobalContext";
import Layout from "../layout/Layout";
import {Helmet} from "react-helmet"
import CarouselImage from "../components/Carousel";
import { Link } from "react-router-dom";

function HomePage() {
  const { products, fetchProducts, loading } = useContext(GlobalContext);

    useEffect(() => {
      fetchProducts();
    }, []);


  return (
    <div>
      {/* NAVBAR */}
      <Layout>
        <Helmet>
            <title>Home - Man Cave</title>
        </Helmet>
        <div className="max-w-7xl mx-auto my-10">
          {/* carousel */}
          <CarouselImage/>

          {/* menu */}
          <div className="flex justify-between my-5">
            <span class="self-center text-2xl text-orange-400 font-semibold">Catalog Products</span>
            <Link to="/product">
              <button 
                type="button" 
                class="text-orange-400 
                   bg-white
                   border-2 border-orange-400
                   hover:bg-orange-100 
                  font-medium rounded-lg 
                  text-sm px-4 py-2 
                  text-center mr-3"
                  >
                  See More
              </button>
            </Link>
          </div>
          {/* list of card products */}
          <div className="grid grid-cols-4 max-md:grid-cols-2 gap-4 pt-2">
            {products.slice(0,4).map((product) => (
              <Product key={product.id} product={product} />
            ))}
          </div>
          <script src="../path/to/flowbite/dist/flowbite.min.js"></script>
      </div>
      </Layout>
      
    </div>
  );
}

export default HomePage;
