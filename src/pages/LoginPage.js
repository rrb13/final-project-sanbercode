import React from "react";
import Layout from "../layout/Layout";
import LoginForm from "../components/LoginForm";

function LoginPage() {
  return (
    <div>
      <Layout>
        <LoginForm />
      </Layout>
    </div>
  );
}

export default LoginPage;
