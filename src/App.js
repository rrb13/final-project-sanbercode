import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import CreatePage from "./pages/CreatePage";
import HomePage from "./pages/HomePage";
import TablePage from "./pages/TablePage";
import UpdatePage from "./pages/UpdatePage";
import RegisterPage from "./pages/RegisterPage";
import LoginPage from "./pages/LoginPage";
import ListProducts from "./pages/ListProductsPage";
import DetailPage from "./pages/DetailPage";
import GuestRoute from "./wrapper/GuestRoute";
import ProtectedRoute from "./wrapper/ProtectedRoute";
import ProfilePage from "./pages/ProfilePage";


function App() {
  return (
    <div className="">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/product" element={<ListProducts/>}/>
          <Route path="/detail/:productId" element={<DetailPage/>} />

          {/* yang bisa diakses saat belum login */}
          <Route element={<GuestRoute/>}>
            <Route path="register" element={<RegisterPage />}/>
            <Route path="login" element={<LoginPage />}/>
          </Route>

          {/* yang bisa diakses kalau sudah login */}
          <Route element={<ProtectedRoute/>}>
            <Route path="/profile" element={<ProfilePage/>}/>
            <Route path="/table" element={<TablePage />} />
            <Route path="/create" element={<CreatePage />} />
            <Route path="/update/:productId" element={<UpdatePage />} />
          </Route>
          
          
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
