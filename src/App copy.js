import { useContext, useEffect, useState } from "react";
import "./App.css";
import Article from "./components/Article";
import CreateForm from "./components/CreateForm";
import Table from "./components/Table";
import UpdateForm from "./components/UpdateForm";
import { GlobalContext } from "./context/GlobalContext";

function App() {
  const { articles, fetchArticles, loading } = useContext(GlobalContext);
  const [editArticle, setEditArticle] = useState(null); // mendapatkan akses data pada artikel yang hendak kita edit
  // Isi dari editArticle
  /*
  {
    "id": 185,
    "name": "Artikel Live Session 10",
    "content": "Ini percobaan pertama live session 10 Post",
    "image_url": "http://api-project.amandemy.co.id//images/pisang.jpg",
    "highlight": true,
    "created_at": "2023-08-06T14:04:09.000000Z",
    "updated_at": "2023-08-06T14:04:09.000000Z",
    "user_id": null,
    "user": null
  }
  */

  useEffect(() => {
    // console.log("Fetch Data.....");
    fetchArticles();
  }, []);

  return (
    <div className="">
      {/* <FormBinding /> */}
      <div className="max-w-6xl mx-auto my-8">
        {editArticle === null ? (
          <CreateForm />
        ) : (
          <UpdateForm
            editArticle={editArticle}
            setEditArticle={setEditArticle}
          />
        )}

        {loading === true ? (
          <h1 className="text-center text-3xl font-bold">Loading ....</h1>
        ) : (
          <div>
            <Table setEditArticle={setEditArticle} />
            <h1 className="text-3xl font-bold text-center">Articles</h1>
            <div className="flex flex-col gap-6">
              {articles.map((article, index) => (
                <Article key={article.id} article={article} />
              ))}
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default App;
