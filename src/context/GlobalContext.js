import axios from "axios";
import { createContext, useState } from "react";
import { useNavigate } from "react-router-dom";


export const GlobalContext = createContext();

export function ContextProvider({ children }) {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(false);

  // const navigate = useNavigate();

  const fetchProducts = async () => {
    setLoading(true);
    try {
      const response = await axios.get(
        "https://api-project.amandemy.co.id/api/final/products"
      );
      // console.log(response.data.data); // berhasil mengakses isi artikel
      setProducts(response.data.data); // berhasil menyimpan artikel pada state
      // articles sudah terisi data dari server
    } catch (error) {
      alert("Terjadi Sesuatu Error");
    } finally {
      setLoading(false); // sukses atau error bakalan berjalan
    }
  };



  return (
    <GlobalContext.Provider
      value={{
        products: products,
        setProducts: setProducts,
        fetchProducts: fetchProducts,
        loading: loading,
        setLoading: setLoading,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
}
