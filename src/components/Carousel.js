import React from 'react';
import { Carousel } from 'antd';
const contentStyle = {
  height: '160px',
  color: '#fff',
  lineHeight: '160px',
  textAlign: 'center',
  background: '#364d79',
};
const CarouselImage = () => (
  <Carousel autoplay>
    <div className='h-[28rem]'>
      <img src="http://api-project.amandemy.co.id/images/sepeda.jpg" class="h-full w-full object-cover" alt=""/>
    </div>
    <div className='h-[28rem]'>
      <img src="https://api-project.amandemy.co.id/images/headphone.jpg" class="h-full w-full object-cover" alt=""/>
    </div>
    <div className='h-[28rem]'>
      <img src="https://api-project.amandemy.co.id/images/earphone.jpg" class="h-full w-full object-cover" alt=""/>
    </div>
    <div className='h-[28rem]'>
      <img src="https://api-project.amandemy.co.id/images/pisang.jpg" class="h-full w-full object-cover" alt=""/>
    </div>
  </Carousel>
);
export default CarouselImage;
