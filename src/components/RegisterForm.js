import axios from "axios";
import React, { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { GlobalContext } from "../context/GlobalContext";
import {useFormik} from "formik";
import * as Yup from "yup";
import {Helmet} from "react-helmet";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

const MySwal = withReactContent(Swal);

const validationSchema = Yup.object({
    name:Yup.string().required("Nama Pengguna Wajib Diisi"), 
    username:Yup.string().required("Username Pengguna Wajib Diisi"),
    email:Yup.string()
        .required("Email Pengguna Wajib Diisi")
        .email("Format email tidak valid"), 
    password:Yup.string().required("Password Wajib Diisi"), 
    password_confirmation:Yup.string().required("Konfirmasi Password Wajib Diisi"), 

})

function RegisterForm() {
  const [input,setInput] = useState({
        name:"", 
        username:"", 
        email:"",
        password:"",
        password_confirmation:"",
    })

    const navigate = useNavigate()
    
    //handle submit yg diinput di form
    const onSubmit = async (values) => {
        try {
            //bikin data user baru
            const response = await axios.post(
                "https://api-project.amandemy.co.id/api/register",
                {
                    name: values.name, 
                    username: values.username, 
                    email: values.email, 
                    password: values.password, 
                    password_confirmation: values.password_confirmation,
                }
            );
            MySwal.fire({
                title: <p>Register Berhasil!</p>,
                icon:'success',
            })
            navigate(-1); // navigate to previous page
        } catch (error) {
                MySwal.fire({
                title: "Oops...",
                icon:'error',
                text:`${error.response.data.info}`
                })
        }
    };

    const {handleChange, values, handleSubmit, errors, touched, handleBlur, setFieldTouched, setFieldValue} = useFormik({
        initialValues: input,
        onSubmit: onSubmit,
        validationSchema: validationSchema,
    });

  return (
    
    <div className="m-5">
        <Helmet>
            <title>Register - Man Cave</title>
        </Helmet>
            <div className="max-w-3xl mx-auto shadow-lg rounded-lg bg-white p-8 my-8">
                <div>
                    <span className="self-center text-xl text-orange-400 font-semibold">Register Form</span>
                </div>
                <div className="max-md:grid-cols-1 max-md:gap-0 pt-5">
                    <div className="mb-6">
                        <label htmlFor="name" className="block mb-2 text-sm font-medium text-gray-900">Nama</label>
                        <input 
                        onChange={handleChange}
                        type="text" 
                        name="name" 
                        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg block w-full p-2.5" 
                        placeholder="Masukan Nama" 
                        value={values.name}
                        onBlur={handleBlur}
                        />
                        { touched.name === true && errors.name != null && (
                            <p className="text-red-500 text-sm">{errors.name}</p>
                        )}
                    </div>
                    <div className="mb-6">
                        <label htmlFor="userName" className="block mb-2 text-sm font-medium text-gray-900">Username</label>
                        <input 
                        onChange={handleChange}
                        type="text" 
                        name="username" 
                        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg block w-full p-2.5" 
                        placeholder="Masukan Nama" 
                        value={values.username}
                        onBlur={handleBlur}
                        />
                        { touched.name === true && errors.userName != null && (
                            <p className="text-red-500 text-sm">{errors.userName}</p>
                        )}
                    </div>
                    <div className="mb-6">
                        <label htmlFor="email" className="block mb-2 text-sm font-medium text-gray-900">Email</label>
                        <input 
                        onChange={handleChange}
                        type="text" 
                        name="email" 
                        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg block w-full p-2.5" 
                        placeholder="Masukan Nama" 
                        value={values.email}
                        onBlur={handleBlur}
                        />
                        { touched.name === true && errors.email != null && (
                            <p className="text-red-500 text-sm">{errors.email}</p>
                        )}
                    </div>
                    <div className="mb-6">
                        <label htmlFor="password" className="block mb-2 text-sm font-medium text-gray-900">Password</label>
                        <input 
                        onChange={handleChange}
                        type="password" 
                        name="password" 
                        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg block w-full p-2.5" 
                        placeholder="Masukan Nama" 
                        value={values.password}
                        onBlur={handleBlur}
                        />
                        { touched.name === true && errors.password != null && (
                            <p className="text-red-500 text-sm">{errors.password}</p>
                        )}
                    </div>
                    <div className="mb-6">
                        <label htmlFor="password" className="block mb-2 text-sm font-medium text-gray-900">Konfirmasi Password</label>
                        <input 
                        onChange={handleChange}
                        type="password" 
                        name="password_confirmation" 
                        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg block w-full p-2.5" 
                        placeholder="Masukan Nama" 
                        value={values.password_confirmation}
                        onBlur={handleBlur}
                        />
                        { touched.name === true && errors.password_confirmation != null && (
                            <p className="text-red-500 text-sm">{errors.password_confirmation}</p>
                        )}
                    </div>

                    
                </div>
                <div className="flex justify-end">
                    <button 
                    // onClick={navigate("/table")}
                    type="submit" 
                    className="text-orange-400  bg-white border-2 border-orange-400 hover:bg-orange-500 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 mx-2 text-center">
                    Cancel</button>
                    <button 
                        onClick={handleSubmit}
                        type="submit" 
                        className="text-white bg-orange-400 hover:bg-orange-500 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 mx-2 text-center">
                        Submit</button>
                </div>
            </div>
        </div>
  );
}

export default RegisterForm;
