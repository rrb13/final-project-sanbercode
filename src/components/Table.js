import axios from "axios";
import React, { useContext, useState } from "react";
import { Link } from "react-router-dom";
import { GlobalContext } from "../context/GlobalContext";
import {Helmet} from "react-helmet";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

const MySwal = withReactContent(Swal);

function Table() {
  const { products, fetchProducts } = useContext(GlobalContext);
  const [query, setQuery] = useState("");

  const keys = ["name","category"]

  const search = (data) => {
    return data.filter(
      (item) => keys.some((key) => item[key].toLowerCase().includes(query))
      )
  }

  const filterProduct = search(products);
  const onDelete = async (id) => {
    try {
      // mengirimkan sebuah delete request
      const response = await axios.delete(
        `https://api-project.amandemy.co.id/api/final/products/${id}`
      );
      fetchProducts();
      MySwal.fire({
        title: <p>Berhasil Menghapus Produk!</p>,
        icon:'success',
        })
    } catch (error) {
                MySwal.fire({
                title: "Oops...",
                icon:'error',
                text:`${error.response.data.info}`
                })
    }
  };
  return (
    
    <section className="max-w-7xl mx-auto my-10">
      <Helmet>
            <title>Table - Man Cave</title>
      </Helmet>
      <h1 className="my-8 text-3xl font-bold text-center">Table Product</h1>
      <div className="flex items-center justify-between pb-4">
        <div class="">
          <label for="table-search" class="sr-only">Search</label>
          <div class="relative mt-1">
              <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                  <svg class="w-4 h-4 text-gray-500 dark:text-gray-400" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                      <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"/>
                  </svg>
              </div>
              <input 
              type="text" 
              id="table-search"
              onChange={(e) => setQuery(e.target.value.toLowerCase())} 
              class="block p-2 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg w-80 bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
              placeholder="Search for items"/>
          </div>
        </div>
        
            <Link to="/create" 
            className="text-orange-400
                bg-white
                  border-2 border-orange-400
                  font-medium rounded-lg
                  text-sm px-4 py-2
                  text-center"
              >
                Create Product
            </Link>

      </div>
      
      <div className="mx-auto w-full my-4">
        <table className="w-full text-sm text-left -text-gray-500">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50">
            <tr>
              <th scope="col" className="px-6 py-3">ID</th>
              <th scope="col" className="px-6 py-3">Nama</th>
              <th scope="col" className="px-6 py-3">Status Diskon</th>
              <th scope="col" className="px-6 py-3">Harga</th>
              <th scope="col" className="px-6 py-3">Gambar</th>
              <th scope="col" className="px-6 py-3">Kategori</th>
              <th scope="col" className="px-6 py-3">Dibuat Oleh</th>
              <th scope="col" className="px-6 py-3">Action</th>
            </tr>
          </thead>
          <tbody>
            
            {filterProduct.map((product) => {
              return (
                <tr key={product.id} className="bg-white border-b hover:bg-gray-50 px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                  <td className="px-6 py-4">{product.id}</td>
                  <td className="px-6 py-4">{product.name}</td>
                  <td className="px-6 py-4">
                    {product.is_diskon === true ? "diskon" : "tidak diskon"}
                  </td>
                  <td className="px-6 py-4">
                    {product.is_diskon === true 
                      ? `${product.harga_diskon_display}` 
                      : `${product.harga_display}`
                      }
                    </td>
                  <td className="px-6 py-4 h-[10rem]">
                    <img src={product.image_url} alt="" className="h-full w-full object-cover" />
                  </td>
                  <td className="px-6 py-4">{product.category}</td>
                  <td className="px-6 py-4">{product.user.username}</td>
                  <td className="px-6 py-4">
                    <div className="flex gap-2">
                      <Link to={`/update/${product.id}`}
                        className="text-white 
                            bg-orange-400 
                            hover:bg-orange-500 
                            font-medium rounded-lg 
                            text-sm px-4 py-2 
                            text-center mr-3 md:mr-0"
                        >
                          Update
                      </Link>
                      <button
                        onClick={() => {
                          onDelete(product.id);
                        }}
                        className="text-orange-400
                          bg-white
                            border-2 border-orange-400
                            font-medium rounded-lg
                            text-sm px-4 py-2
                            text-center"
                      >
                        Delete
                      </button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </section>
  );
}

export default Table;
