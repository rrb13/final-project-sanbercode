import React, {useEffect, useState} from 'react';
import axios from 'axios';

function Profile() {
    const [profile, setProfile]= useState({});
    console.log(profile)
    const fetchProfile = async () => {
        try {
            const response = await axios.get(
                "https://api-project.amandemy.co.id/api/profile",
                {
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem("token")}`
                    }
                }
            );
            setProfile(response.data.data)
            
        } catch (error){
            alert("Terjadi Sesuatu Error")
        }
    }

    useEffect(() => {
        fetchProfile();
        console.log(profile)
    },[])

  return (
    <div class="grid grid-cols-3 max-w-7xl mx-auto my-10 gap-2 max-md:grid-cols-1 max-md:gap-0 pt-2">
            <div class="bg-white border border-gray-200 rounded-xl max-md:rounded-b-none h-[28rem]">
                <img class="rounded-xl max-md:rounded-b-none h-full w-full object-cover" src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png" alt="" />
            </div>
            <div class="col-span-2 grid content-start bg-white border border-gray-200 rounded-xl max-md:rounded-t-none p-5">
                <h1 className='text-2xl font-semibold'>Biodata Diri</h1>
                <div class="relative overflow-x-auto">
                    <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                        <tbody>
                            <tr class="bg-white ">
                                <th scope="row" class="px-6 py-4 font-medium text-lg text-gray-900 ">
                                    Nama
                                </th>
                                <td class="px-6 py-4 text-lg">
                                    {profile.name}
                                </td>
                            </tr>
                            <tr class="bg-white ">
                                <th scope="row" class="px-6 py-4 font-medium text-lg text-gray-900 ">
                                    Username
                                </th>
                                <td class="px-6 py-4 text-lg">
                                    {profile.username}
                                </td>
                            </tr>
                            <tr class="bg-white ">
                                <th scope="row" class="px-6 py-4 font-medium text-lg text-gray-900 ">
                                    Email
                                </th>
                                <td class="px-6 py-4 text-lg">
                                    {profile.email}
                                </td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
            </div>

    </div>
  )
}

export default Profile