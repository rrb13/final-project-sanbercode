import React from "react";
import { Link } from "react-router-dom";

//function untuk cek apakah ada diskon atau tidak
function IsDiskon({product,is_diskon}){
    console.log(is_diskon);
    console.log(product);
    if(is_diskon){
        return (
            <div>
                <h5 className="mb-2 text-md font-semibold tracking-tight text-gray-900 line-through">{product.harga_display}</h5>
                <h5 className="mb-2 text-md font-semibold tracking-tight text-red-400">{product.harga_diskon_display}</h5>
            </div>
        )
    } else {
        return <h5 className="mb-2 text-lg font-semibold tracking-tight text-gray-900">{product.harga_display}</h5>
    }
}


function Product({product}) {
    return (
        <div className="bg-white border border-gray-200 rounded-lg shadow">
                <Link to={`/detail/${product.id}`}>
                    <img 
                    className="w-full aspect-square object-cover rounded-t-lg" 
                    src={product.image_url} 
                    alt="" />

                    <div className="p-2">
                    
                        <h1 className="mb-2 text-lg font-bold tracking-tight text-gray-900">{product.name}</h1>
                        <IsDiskon is_diskon={product.is_diskon} product={product}/>
                        <h5 className="mb-2 text-md font-semibold tracking-tight text-blue-400">{product.stock}</h5>
                    
                    </div>
                </Link>
                    
            </div>
    )
}

export default Product;